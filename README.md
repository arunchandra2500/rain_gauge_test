Automatic Rain Gauge Station

LoRaWAN based Automatic Rain Gauge Station is a system which measure the Total rainfall and transmits the data at regular intervals.


LoRaWAN transmission is every 15 minutes.

The application code is written on top of the I-cube-lrwan package by STM32.
