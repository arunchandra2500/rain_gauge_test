/*
 * raingauge_station.h
 *
 *  Created on: 16-May-2022
 *      Author: arun
 */

#ifndef RAINGAUGE_STATION_H_
#define RAINGAUGE_STATION_H_


#define BATT_POWER    	1

typedef struct {


	uint16_t rainfall;

    uint16_t batteryLevel;

    uint16_t totalrainfall;

    uint16_t downlinkRainfall;


}rainfallData_t;

uint16_t readBatteryLevel(void);

void rainGaugeInterruptEnable();
void rainGaugeTips();
uint16_t getAccumulatedRainfall();

void enable(uint8_t);
void disable(uint8_t);
void raingaugeStationInit();
void readRaingaugeStationParameters(rainfallData_t *sensor_data);
void raingaugeStationGPIO_Init();


#endif /*RAINGAUGE_STATION_H_ */
